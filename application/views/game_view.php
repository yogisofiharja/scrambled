
<style type="text/css">
	.col-centered{
		float:none;
		margin:0 auto;
	}
</style>
<div class="fh5co-features">
	<div class="container">
		<div class="row" id="instruction">
			<div class="col-6 col-offset-3">
				<h1>How To Play</h1>
				<ul>
					<li>Type your name, then press "Enter" or click "Start"</li>
					<li>You'll see a scrambled word, then rearrange the characters to make the correct word</li>
					<li>Type your answer, press "Enter" or click "Send"</li>
					<li>Every correct word, you'll get 1 point, and you will miss 1 point for wrong word</li>
					<li>Enjoy the game, and share it to your friends</li>
				</ul>
				<input type="text" name="name" id="name" placeholder="Your Name">
				<label id="name_alert"></label>
				<br>
				<br>
				<button id="start" class="btn btn-primary">Start Game</button>&nbsp;<br><br>
			</div>
		</div>
		<br>
		<div class="row">
			<div id="arena" class="col-3 text-center">
				<h3>Your Score :</h3>
				<label id="score">0</label><br/><br>
				<label id="scrambled"></label><br/>
				<input type="text" id="answer" name="answer">
				<br>
				<br>
				<button id="send" class="btn btn-info">Send</button>
				<br>
				<br>
				<div id="alert_span"></div>
			</div>

		</div>
	</div>
</div>
<!-- modal -->
<div class="modal fade" id="result" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p id="final_score"></p>
        <p>
        <ul class="nav nav-pills">
        	<li class="presentation">
        	<a href="https://twitter.com/share" class="twitter-share-button" data-text="" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> </li>
        	<li><div class="fb-share-button" data-href="https://scrambled.svggestore.com/" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fscrambled.svggestore.com%2F&amp;src=sdkpreparse">Share</a></div></li>
        </ul>
        
        
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info btn-sm" id="close" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary  btn-sm" id="save">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	window.score = 0;
	$(document).ready(function(){
		$('#arena').hide();
	});
	$('#close').click(function(){
		location.reload();
	});
	$('#save').click(function(){
		var data = {name:$('#name').val(), score:window.score};
		console.log(data);
		$.ajax({
			url:'<?php echo base_url("main/save");?>',
			data:data,
			method:'POST',
			dataType:'JSON',
			success:function(result){
				if (result==true){
					$('#alert_span').html('<button class="btn btn-success">Thanks '+window.name+'</button>');
					$('#result').modal('hide');
					setTimeout(function(){
						$('alert_span').html('');
					}, 5000);
					setTimeout(function(){
						location.reload();
					}, 5000);
				}
			}
		});
	});
	$('#start').click(function(){
		if($('#name').val()==''){
			$('#name_alert').html('<button class="btn btn-warning ">You have to type your name :)</button>');
		}else{
			start();	
		}
	});
	$('#send').click(function(){
		checking()
	});
	$('#answer').keypress(function(e){
		if(e.which==13){
			checking();
		}
	});
	$('#name').keypress(function(e){
		if(e.which==13){
			start();
		}
	});	
	function start(){
		$('#arena').show();
		$('#instruction').hide();
		$('#answer').focus();
		window.name = $('#name').val();
		window.words = [];
		$.ajax({
			url:"<?php echo base_url('main/get_words');?>",
			method:"POST",
			dataType:"JSON",
			success: function(result){
				window.words = result;
				GetRandomWord(window.words, words.length-1)
			}
		});
	}
	function checking(){
		var answer = $('#answer').val();
			if(answer===window.selectedWord.word){
				window.score = window.score+1;
				$('#alert_span').html('<button class="btn btn-success">Correct!</button>');
				
			}else{
				window.score = window.score-1;
				$('#alert_span').html('<button class="btn btn-danger">Wrong!</button>');
			}
			setTimeout(function(){
				$('#alert_span').html('')
			},1000);
			$('#answer').val("");
			$('#score').html(window.score);
			GetRandomWord(window.words, words.length-1);
	}
	function GetRandomWord(data, counter){
		if(counter<1){
			$('.modal-title').html('Congratulation '+window.name);
			$('#final_score').html('Your Final Score is '+window.score);
			$('#result').modal('show');
		}else{
			var index = Math.floor((Math.random() * counter) + 0);
			window.selectedWord = window.words[index];
			$('#scrambled').text(window.selectedWord.word.shuffle());
			window.words.splice(index,1);
		}
	}

	String.prototype.shuffle = function () {
		var a = this.split(""),
		n = a.length;

		for(var i = n - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
		}
		return a.join("");
	}
</script>
<style type="text/css">
	.fb_iframe_widget iframe{
		margin-top:-4px;
	}
</style>