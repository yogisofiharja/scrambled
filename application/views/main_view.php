<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<!-- Google Webfont -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700|Unica+One' rel='stylesheet' type='text/css'>
	<!-- Themify Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/themify-icons.css')?>">
	<!-- Icomoon Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/icomoon-icons.css')?>">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css')?>">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css')?>">
	<!-- Easy Responsive Tabs -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/easy-responsive-tabs.css')?>">
	<!-- Theme Style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">

	
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/modernizr-2.6.2.min.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
</head>
<body>

	<!-- Header -->
	<header id="fh5co-header" role="banner">

		<!-- Logo -->
		<div id="fh5co-logo" class="text-center">
			<a href="<?php echo base_url(); ?>">
				<h2>Scrambled Word</h2>
			</a>
		</div>
		<!-- Logo -->

		<!-- Mobile Toggle Menu Button -->
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
	</header>
	<!-- Header -->

	<!-- Main -->
	<main role="main">

		<?php 
		$this->view($page);
		?>

	</main>
	<!-- Main -->

	<!-- Footer -->
	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p class="fh5co-copyright">&copy; Yogi Sofi Harja</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer -->
	
	<!-- Go To Top -->
	<a href="#" class="fh5co-gotop"><i class="ti-shift-left"></i></a>
	

	
	<!-- jQuery Easing -->
	<script src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url('assets/js/owl.carousel.min.js')?>"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url('assets/js/jquery.magnific-popup.min.js')?>"></script>
	<!-- Easy Responsive Tabs -->
	<script src="<?php echo base_url('assets/js/easyResponsiveTabs.js')?>"></script>
	<!-- FastClick for Mobile/Tablets -->
	<script src="<?php echo base_url('assets/js/fastclick.js')?>"></script>
	<!-- Velocity -->
	<script src="<?php echo base_url('assets/js/velocity.min.js')?>"></script>
	<!-- Main JS -->
	<script src="<?php echo base_url('assets/js/main.js')?>"></script>
	<!-- game JS -->
	<script src="<?php echo base_url('assets/js/game.js')?>"></script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=251932934930860";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<?php
	if($page=="gallery_detail_view")
	{
		?>
		<script src="<?php echo base_url('assets/js/masonry.pkgd.min.js')?>"></script>	
		<script>
			$('.grid').masonry({
						// options
						itemSelector: '.grid-item',
						columnWidth: 200
					});
				</script>			
				<?php
			}
			?>
			
		</body>
		</html>
