<div class="row">
  <div class="col-md-12">
    <div class="box">
      <!-- Box Header-->
      <div class="box-header">
        <h3 class="box-title">Attempt</h3>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body pad">
      <?php
        if($this->session->flashdata('word_alert')!=''){
          echo $this->session->flashdata('word_alert');
        }
      ?>
        
        <table id="list_table" class="table table-bordered table-hover">
          <?php
          ?>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
              <th>Score</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach($list_attempt as $attempt){
              ?>
              <tr>

                <td><?php echo $attempt->name;?></td>
                <td><?php echo $attempt->date;?></td>
                <td><?php echo $attempt->score;?></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
              <th>Score</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <!-- /.col-->
</div>


