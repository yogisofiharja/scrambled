<div class="row">
  <div class="col-md-12">
    <div class="box">
      <!-- Box Header-->
      <div class="box-header">
        <h3 class="box-title">Words</h3>
        <div class="pull-right box-tools">
          <button class="btn btn-primary btn-sm" aria-hidden="true" data-toggle="modal" data-target="#addWord">+ Add Word</button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body pad">
      <?php
        if($this->session->flashdata('word_alert')!=''){
          echo $this->session->flashdata('word_alert');
        }
      ?>
        <!-- Modal -->
  <div class="modal fade" id="addWord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add Word</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo base_url('admin/words/add_word');?>">
            <div class="form-group">
              <label for="word" class="col-sm-3 control-label">Word</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="word" placeholder="word" name="word">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" >
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- end modal -->
        
        <table id="list_table" class="table table-bordered table-hover">
          <?php
          ?>
          <thead>
            <tr>
              <th width="20%">Words</th>
              <th width="15%">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach($list_words as $words){
              ?>
              <tr>

                <td><?php echo $words->word;?></td>
                <td align="center">
                  <a href="<?php echo base_url("admin/words/delete/".$words->id);?>"> <i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
          <thead>
            <tr>
              <th width="20%">Words</th>
              <th width="15%">Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <!-- /.col-->
</div>


