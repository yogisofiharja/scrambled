<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Words extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('username')){
			$this->session->set_flashdata('error','Please login to view this page');
			redirect('admin/auth');
		}else{
			$this->load->model('model_words');
		}
	}
	public function index()
	{
		$data['words']		= "active";
		$data['title'] 		= "Scrambled Words";
		$data['page'] 		= "admin/words";
		$data['list_words'] = $this->model_words->get_all_words();
		$this->load->view('admin/main_view', $data);
	}

	public function add_word(){
		$word = new Model_words();
		$word->word = $this->input->post('word');
		if($word->add_word()){
			$alert = '<div class="alert alert-info alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Word has been saved
                </div>';
			
		}else{
			$alert = '<div class="alert alert-info alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Word has been saved
                </div>';
		}
		$this->session->set_flashdata('word_alert',	$alert);
		redirect('admin/words');
	}

	public function edit($id){
		$data['word'] = $this->model_words->get_word_by_id($id);
		$this->load->view('', $data);
	}

	public function delete($id){
		$this->model_words->delete($id);
		redirect('admin/words');
	}
}
