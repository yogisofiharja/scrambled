<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('model_auth');
    }

	public function index()
	{
		if ($this->session->userdata('username')!='') {
			redirect('admin/words'); 
		}else{
			$this->load->view('admin/login');
		}
	}

	public function login()
	{
		$data = array('username' => $this->input->post('username', TRUE),
			'password' => md5($this->input->post('password', TRUE))
			);
		$hasil = $this->model_auth->user_checking($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['id'] = $sess->id;
				$sess_data['username'] = $sess->username;
				$this->session->set_userdata($sess_data);
			}
			redirect('admin/auth');

		}
		else {
			$this->session->set_flashdata('error','Please check your username and password.');
			$this->load->view('admin/login');
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		session_destroy();
		redirect('admin/auth');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */