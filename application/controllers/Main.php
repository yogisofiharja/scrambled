<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('model_words');
		$this->load->model('model_play');
	}
	public function index()
	{
		$data['home'] 		= "active";
		$data['title']		= "Scrambled Word";
		$data['page']		= "game_view";
		$this->load->view('main_view', $data);
	}

	public function get_words(){
		$words	= $this->model_words->get_all_words();
		echo json_encode($words);
	}

	public function save()
	{
		$data	= $this->input->post();
		// print_r($content);die;
		$save 		= $this->model_play->save($data);
		if($save)
		{
			echo 'true';
		}else{
			echo 'false';
		}
	}
}
