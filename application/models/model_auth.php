<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_auth extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function user_checking($data) {
		$query = $this->db->get_where('auth', $data);
		return $query;
	}

}

?>