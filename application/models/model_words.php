<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_words extends CI_Model
{
    var $word = '';

    function __construct()
    {
        parent::__construct();

    }

    function get_all_words()
    {
    	$this->db->select('*');
        return $this->db->get('words')->result();
    }

    function get_word_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('words')->row();
    }

    function update_word($id, $data)
    {
        
    	$this->db->where('id', $id);
    	$update = $this->db->update('words', $data);
    	if ($update)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    function add_word()
    {
        $data = array(
            'word' => $this->word
        );
    	$this->db->insert('words', $data);
    	return ($this->db->affected_rows() != 1) ? false : true;
    }
    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('words');
    }
}
