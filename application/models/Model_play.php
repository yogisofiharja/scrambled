<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_play extends CI_Model
{
    var $name = '';
    var $score = '';

    function __construct()
    {
        parent::__construct();

    }

    function get_all_attempt()
    {
    	$this->db->select('*');
        return $this->db->get('attempt')->result();
    }

    function get_attempt_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('attempt')->row();
    }

    function save($data)
    {
    	$this->db->insert('attempt', $data);
    	return ($this->db->affected_rows() != 1) ? false : true;
    }
    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('attempt');
    }
}
